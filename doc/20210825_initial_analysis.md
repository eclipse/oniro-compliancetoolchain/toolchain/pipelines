---
title: Initial Analysis on Compliance Pipelines
author: Alberto Pianon <pianon@array.eu>
date: 2021-08-25
lastmod: 2021-08-25
SPDX-License-Identifier: CC-BY-SA-4.0 AND LicenseRef-Trademark-Notice
SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>
---

# 1. Context Summary

This report is mainly intended for AllScenariOS compliance pipeline developers and implementers, but aims at being understandable also by legal experts and auditors that need to "consume" the compliance pipeline process and to provide feedback on the process design and implementation.

Thus, some general notions should be summarized first. You can safely skip the parts you are already familiar with.

## 1.1. Yocto, Bitbake, Poky

AllScenariOS uses **Yocto** as build environment. Yocto is based on OpenEmbedded Build System and is targeted at building firmware (i.e. the operating system and the application layer) for embedded devices.

The main command used to *"cook"* firmware images is **bitbake**. There is also a reference distribution named "**poky**", consisting in the build system plus a set of layers to be used as a basis to build embedded software distributions.

**Yocto has not a package manager** like Debian or RedHat, but uses a build system based on **stratified layers of recipes that may interfere with each other**, to allow a greater flexibility; however, such **greater flexibility comes at a cost for compliance work**, which has to face additional issues, compared to systems based on package managers (see below).

## 1.2. AllScenariOS and Yocto

Generally, Yocto is used to build single independent projects, each targeted at providing a single specific application through a dedicated hardware device.

Conversely, **AllScenariOS uses Yocto** in a different context and **for a different (and actually, opposite) goal**: instead of having each smart device maker cook its own linux/zephyr/freertos distribution with Yocto, the aim of AllScenariOS  is to create a single standard operating system for embedded devices, developed and maintained by a community, that can be built for many different hardware devices for many different use cases, containing only software components that are both technically and legally "safe", and that any device maker can easily use to build its own specific applications.

Obviously, this goal adds even **more complexity** to the related **compliance work**, compared to an "average" Yocto project aimed at providing only a specific application in a single hardware device. Therefore, **some of the issues that we have to deal with to design the compliance workflow and pipelines for AllScenariOS are "new" and specific to AllScenariOS itself**.

## 1.3. AllScenariOS Complexity: Targets, Recipes, Layers, Manifest

Let us try to briefly describe this complexity, in order to understand how it affects compliance work and the design of the compliance CI/CD pipelines.

### 1.3.1. Targets: A Multi-Dimensional Matrix of Different Flavours, Machines and Images

First, **targets**: an "average" Yocto project is intended to be built around a specific kernel (linux, freeRTOS, etc.), for a single hardware board, and is aimed at creating a single specific firmware image, containing a fixed set of software components. Instead, **AllScenariOS has a multi-dimensional matrix of possible build targets**: one can choose between different "flavours" depending on the kernel they choose (linux, zephyr, freeRTOS), but also between different target machines (with an always growing list of supported hardware boards), different target firmware images (including different subsets of software components), and, in the near future, also different "variants" for each target image (optimized for debugging, or for production, etc.).

Not only not all source components are included in all possible targets, but the same upstream source component may be modified in different ways depending of the final target.

This implies that if one tried to "package" all software components for all possible targets in order to analyze them (both for license compliance and security reasons), they would end up with an unmanageable number of variants of the same packages to analyze (both at source and at binary level).

> as a consequence, the first "new" issue of AllScenariOS is that **aggregation is needed**, in order to **represent all possible variants** of the same software component **in a single package schema**, while **tagging in an appropriate way source files or source modifications that are specific only to certain targets** (incidentally, that is why "*post-mortem*" software packaging functionalities provided by Yocto -- eg. the archiver class -- cannot be used as such in AllScenariOS, and new tools like TinfoilHat are being developed).

### 1.3.2. Recipes: Variants Across Different Targets and Releases, and Missing Unique Identifiers

Another source of additional complexity is how recipes work in Yocto and how they are used in AllScenariOS.

AllScenariOS includes both 3rd party and 1st party **source components**.
- first party source components are hosted and developed in git repos in git.ostc-eu.org
- third party source components may come from various sources (github, gitlab, sourceforge, tarball downloads, etc)

All source components are integrated through Yocto **recipes** that contain data and instructions for bitbake:

- to fetch sources of the component (let's call them *"upstream sources"*)
- (if sources come from a VCS like git or svn) to checkout to a certain revision or pick up the latest available one, depending on recipe settings (in the first case, version is said to be *"pinned"*)
- to possibly apply some patches (bug fixes, CVE fixes, hardware specific patches, etc.) and/or add config files, scripts, etc. (let's call them *"downstream files"*), also depending on the target machine or other factors (so there may be **downstream files** that are **used only in certain builds** and not in others)
- to build the component for one or more target machines

**Recipes** are usually identified by the same name and version and/or VCS revision tag of the upstream source component they integrate (like `curl-7.69.1`); there is also an additional revision tag (like in `curl-7.69.1-r0`) but it is just an arbitrary incremental number specific to a certain build (so it is not globally unique) and usually it remains `r0` (more info see Yocto Mega Manual's [Glossary](https://www.yoctoproject.org/docs/current/mega-manual/mega-manual.html#var-PR)).  

Generally, when downstream files (patches, config files, scripts, etc.) are added, removed or modified in a recipe, such changes are not reflected in a version change (as it otherwhise happens with Debian package versions).

Only *within the same build*, a certain recipe identified by a certain `name-version-revision` is unique; but across different builds (and even across different releases of the same build), we may well have (and actually we do have, in AllScenariOS) **recipes that share the same `name-version-revision` but are different from one another** (meaning that, generally, at least the upstream source should be -- hopefully! -- the same, but some downstream files may be different).

Moreover, **recipes can be overriden by other recipes** in other layers (see below); generally, such overrides should concern (as far as sources are concerned) only downstream files. In any case this means the very same recipe, put in different contexts (i.e. different layer structures) may use a different set of source files.

Normally, this is not an issue in an "average" Yocto project, precisely because Yocto is generally used to develop specific independent projects for single specific targets. Usually, therefore, it is enough that a given recipe and its identifier are unique in the context of the build for that specific target.

> However, this is not enough in the context of AllScenariOS. In order to be able to do a software composition analysis and related compliance work, we need to **add a source component identifier that is unique across the whole target matrix and also across different releases**.

The current solution to achieve so is an hashing mechanism based on the hashes (git commit or file checksum, depending on the source type) of all source files/archives/repos included in the recipe's variants within the same release or development snapshot.

### 1.3.3. Layers

In Yocto, **recipes are grouped in layers** (usually named `meta-something`). As mentioned above, a layer can **override** things in another layer, so **adding or removing a layer may have side effects on other layers**.

Layers can contain both metadata / config parameters, but also actual source code that performs certain operations during build, and downstream files (patches, config files, scripts etc.) referred to in its recipes (or also in the recipes from other layers, in order to apply some overrides to such former recipes).

A layer may be aimed at supporting a specific hardware, providing a given group of functionalities or a specific application, etc.

Layers in AllScenariOS can be grouped as follows:

- base layers of the Yocto project
- other third party layers (by hardware vendors, application providers, etc)
- first party layers (by OSTC and other members of the WG); the main 1st party layer is `meta-ohos`.

All layers used in AllScenariOS are developed in git repos. First party layer repos are hosted at git.ostc-eu.org.

### 1.3.4. The Manifest

The **manifest** is what brings everything together. It is an XML file containing an ordered list of layers, the git repos where they can be fetched, and (possibly) the specific revision of each layer (if revision is specified, the layer is said to be "pinned"; otherwise, the last available version will be fetched from the corresponding git repo).

It goes without saying that **a small change in the manifest** (eg. adding a single line that incorporates a whole new layer, or changing the revision of an existing layer) **may lead to significant changes in the final software composition** (new or different components, etc.)

It should be noted that starting from metadata contained in AllScenariOS manifest it is possible to build all possible targets in the target matrix.

Like all first party components, also AllScenariOS manifest is "developed" in a git repo hosted at git.ostc-eu.org.

# 2. How Build Pipelines are Currently Designed in AllScenariOS

The complex structure described above implies that the final build results across the whole target matrix may be affected:

a) by a change in the source code component integrated by a recipe

b) by a change in the recipe/layer that integrates it

c) by a change in another layer that overrides that recipe

d) by a change in the manifest that adds or removes (or changes the revision of) a certain layer

Given how Yocto is designed (recap: it does *not* have a package manager, but different layers of recipes that may interfere with each other) the impact of each of such changes can be be tested only by building all the possible targets in the target matrix, after that specific change has been made.  

So, CI/CD build test pipelines need to be run against every commit and merge request to the main branch(es):

1. in the manifest repo,
2. in all first party layer repos, and
3. in all first party source repos.

This means that CI/CD pipelines that build all possible targets in the matrix need to be run very frequently, <!-- FIXME: how frequently? Get stats! --> which in turn implies a huge machine workload. Luckily, such pipelines may leverage Yocto SSTATE mechanism and use a shared build cache, so all previous build artifacts can be always reused and only things that actually changed need to be reprocessed and/or rebuilt.

Another important issue is that in order to be able to test builds in cases 2 and 3 above (changes to layer or source repos), the build pipeline cannot simply fetch the manifest, then fetch layers based on manifest metadata, and then fetch sources based on layer metadata (this would work just for case 1):

- in case 2 (layer changes), the pipeline needs to fetch the manifest and the layers, and then checkout the cloned git repo of that specific layer that has changed to the commit that needs to be tested, and only then fetch source repos and start the build;
- in case 3 (source component changes), the pipeline needs to fetch the manifest and the layers, and then use a dedicated tool provided by Yocto to override the specific recipe which integrates the changed component, so that it points to the component's source revision that needs to be tested.

# 3. Compliance Pipelines

## 3.1. The Main Similarities and Differences Between Build and Compliance Pipeline Requirements

Given how AllScenariOS project is designed, if we want to implement a real continuous compliance workflow, we should design compliance CI/CD pipelines modeled on current build CI/CD pipelines.

This means:
- that we generally need to always build all targets in the target matrix to get the actual software composition "generated" by each change that triggers the pipeline
- that we need to run the compliance pipeline in all the cases described above for build pipelines (i.e. manifest change, layer change, source component change),
- that compliance pipelines are therefore expected to be run very frequently
- that we also need to apply the same logic to handle the different cases described in sec.2

However, there are some significant differences that we need to take into account:

- current build pipelines build every target in a separate job, while in compliance pipelines we need to build all targets together, in order to be able to aggregate data, as explained in sec. 1.3.1 above
- the "complete" compliance pipeline cannot be run too frequently, not only because it relies on tools (like ScanCode and Fossology license scanners) that may have long execution times (this could be solved by adding more hardware horsepower), but also because it would otherwise unnecessarily overload the audit team work (the compliance process is an asyncronous process, because automated scanner results need to be validated in Fossology by the audit team, and the final results are collected by the compliance pipeline at a later stage; so continuously feeding Fossology with new source uploads may technically work, but it would be unmanageable for the audit team).

## 3.2. The Prospected Solution

After some discussion at the 2021-08-20 meeting, we came up with the following possible solution.

There should be a first part of the pipeline, that just builds all images (leveraging the existing private SSTATE cache, if possible) and runs only aliens4friends’ debian matcher tool on the upstream source of each new software package/recipe (we do not care about yocto patches at this stage). If there is a good debian matching (eg. > 80%) or if the package is included in a whitelist, there is no need to proceed further (the new package will be scanned at a later stage, eg. via a periodically scheduled pipeline every 2 weeks). If there is a bad (or no) debian matching, or if the package is included in a blacklist, the second part of the pipeline (the “real” compliance pipeline) will be triggered. This first pipeline should be triggered at every commit on meta-ohos develop branch, as well as on every MR into the develop branch, and provide artifacts (a json file?) that can be read by developers to assess which component changes have been introduced by their commit(s) and to check if the second (the “real”) compliance pipeline has been triggered and why.

> internal note: this is a good reason to keep both the “old” debian matcher and the “new” debian snapshot matcher in a4f: the first one (less accurate, not reproducible, but faster) could be used in the first part of the pipeline, while the second one (more accurate, always reproducible, but significantly slower) could be used in the second part of the pipeline.

The second pipeline (the “real” one) will perform all the steps of aliens4friends’ workflow, and will be triggered:

- by the fist pipeline; but also
- by a periodic scheduler; and
- manually by developers when needed (great power means great responsibility: use with care!)

There will be also another pipeline, running periodically (eg. nightly) that will perform just the final 2 steps of aliens4friends’ workflow (fossy and harvest), in order to update json stats for the dashboard and therefore to monitor audit work progress on Fossology

## 3.3. TBD: Environment Hashing

All parts of the pipeline would require some sort of “environment hashing” (modeled on SSTATE) in order to have unique identifiers of what we are checking, scanning and/or auditing. This topic needs to be further explored.
## 3.4. TBD: Reproducibilty

Both for practical (debugging) and legal (evidence keeping) reasons, ideally every scan done by each complete pipeline should be always reproducible.

In practice, this is not always the case, because AllScenariOS needs to fetch most of its components (sources, layers) from third party git repos or Internet sites, which at some point may delete old revisions or may even become unavailable. Moreover, also in first party repos some old revisions may be deleted at times, mainly because of git rebase operations, that could be only partially limited by policy, because in certain cases they may be unavoidable.

For first party repositories, some technical solution should be implemented so that at least the revisions used in *tagged releases* are preserved and never deleted.

For third party repositories, there is obviously no solution.

Therefore, an internal method and an internal policy to keep snapshots of all scanned sources (at least for every *tagged release*, and generally whenever it is legally advisable) should be designed and implemented.
