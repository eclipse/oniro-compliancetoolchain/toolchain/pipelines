---
title: Report on build matrix changes - Jul-Aug 2022
author: Alberto Pianon <pianon@array.eu>
date: 2022-08-17
lastmod: 2022-08-17
SPDX-License-Identifier: CC-BY-SA-4.0 AND LicenseRef-Trademark-Notice
SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>
---

# 1) Clang vs. Gcc

Oniro can now be built also with clang/LLVM instead of GNU GCC, and new build pipelines build both. This doubles the total number of build targets. So the question is: do we need to build also clang images in our IP compliance pipelines, which will likely double their execution time? The answer to such question depends on another question: may images built with clang contain a different set of software components from images built with gcc (apart from runtime libraries - which is indeed a difference, anyway)?  If final *image* software composition is expected to be different, the answer to the first question would be "yes".

In this respect, in a discussion in Mattermost @bero noted:

> When building stuff normally, clang vs. gcc doesn't change anything other than runtime libraries - but of course if something is built with static linking, there can be differences (e.g. implementation from libgcc.a vs. implementation from libcompiler-rt.*.a pulled into the final binary)
>
> And of course there's always a (highly unlikely) possibility of some code doing the likes of

```c
#ifdef __clang__
#include <some/library>
#else
#include <some/other/library>
#endif
```

> to work around bugs or to use special optimizations
>
> So I think if we want to be on the really safe side, we need to enable it, but if we just want to be reasonably sure, no need .
>
> Probably it's not something we need to run all the time, but we should run it once in a while (e.g. before a release)

In any case, as long as clang binary images (at least some of them, depending on the target machine) may be part of the final release, they definitely need IP check, so **clang images should be built and scanned in IP Compliance pipelines**, even if not necessarily with the same frequency of "regular" gcc builds.

# 2) ABI compliance check

New build pipelines implement also an ABI compliance check through `check-abi` bitbake class. 

In this respect, in a discussion in Mattermost @landgraf noted:

> abi compliance class doesn't alter any binaries/software composition and it's not part of the default (partner facing) build even (that's one of the reason I've moved it out of default.xml and it's in ci.xml)

So, since abicheck class is needed just for ABI compliance check and it should not alter the final software composition, **we do not need to add abicheck to IP compliance pipelines**.

# 3) Identification of binary artifacts requiring IP check

Since some binary artifacts will be part of the final GA release, it is important to identify them. To that purpose, `CI_ONIRO_JOB_ARTIFACTS` variables may be used, but we need to be sure that artifacts listed there will be all part of the release and do not include files needed just for development/debugging purposes.

In this respect, in a discussion in Mattermost @landgraf specified:

> abi checher adds some files into artifacts. They are xml files which describes binary/elf images (their ABI). They will be published in our repo which contains release info

This may revert the conclusion reached in the previous point 2). However, artifacts created by ABI checker are just metadata collected from binary files through a standard automated tool (`abidw`)

In this respect, @piana noted:

> If it's just automated stuff collected as a by-product of making software development or system maintenance, without any substantial investment on collecting, vetting, indexing, publishing it, there is no copyright nor database rights, just facts. I'm quite confident this is the case. 

So we do not need to do any IP check on ABI check artifacts nor worry about licensing them in any way. Therefore **we should do IP checks on all artifacts listed in `CI_ONIRO_JOB_ARTIFACTS` but ABI check artifacts**.


# Support for avenger96 boards - clarification.

In a discussion in Mattermost @andrei specified that avenger96 board support was dropped only on Linux, while such board is still supported on Zephyr. The [issue](https://github.com/dh-electronics/meta-av96/issues/9) opened by @rahulmohang does not affect Zephyr.

So we can **keep avenger96 zephyr target(s) in IP Compliance pipelines**.

# Extra images - clarification

Currently, in build pipelines `-extra` images are built just for Seco targets, and they are built using `.build-linux` job template, while all `-test` images use `.build-wic-image`. Practically the only differences are the `rm-work` class (enabled in `.build-linux`) and `CI_ONIRO_JOB_ARTIFACTS`. 

@andrei confirm that this is just something that needs cleanup. So **in IP compliance pipelines we can build `-extra` images in the same way we build all the other images, without need of any specific configuration**.

