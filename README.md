<!--SPDX-License-Identifier: Apache-2.0
Copyright Peter Moser <p.moser@noi.bz.it>
Copyright Alberto Pianon <pianon@array.eu>
-->

# IP Compliance Pipelines

**Table Of Contents**

<!-- TOC -->

- [IP Compliance Pipelines](#ip-compliance-pipelines)
  - [1. Introduction: Context, Analysis and Design of the Pipelines](#1-introduction-context-analysis-and-design-of-the-pipelines)
    - [1.1. Package and metadata aggregation](#11-package-and-metadata-aggregation)
    - [1.2. Pipeline triggers](#12-pipeline-triggers)
    - [1.3. Pipeline types and run frequency](#13-pipeline-types-and-run-frequency)
  - [2. Docker images](#2-docker-images)
  - [3. External bash scripts](#3-external-bash-scripts)
    - [3.1. Oniro scripts](#31-oniro-scripts)
    - [3.2. Leda scripts](#32-leda-scripts)
  - [4. Modular pipeline definition: yaml files](#4-modular-pipeline-definition-yaml-files)
    - [4.1. Oniro](#41-oniro)
    - [4.2. Leda](#42-leda)
  - [5. Compliance Pipelines and Repo Mirroring](#5-compliance-pipelines-and-repo-mirroring)
  - [6. Current implementation (updated 2023-04-03)](#6-current-implementation-updated-2023-04-03)
    - [6.1. Mirrors](#61-mirrors)
    - [6.2. Pipeline](#62-pipeline)
  - [7. GitLab Setup](#7-gitlab-setup)
    - [7.1. Server](#71-server)
      - [7.1.1. Oniro](#711-oniro)
      - [7.1.2. Leda](#712-leda)
    - [7.2. GitLab Runner Configuration](#72-gitlab-runner-configuration)
    - [7.3. GitLab Badge to link results to the Aliens4Friends SCA dashboard](#73-gitlab-badge-to-link-results-to-the-aliens4friends-sca-dashboard)
    - [7.4. Known limitation: Single branches only](#74-known-limitation-single-branches-only)
    - [7.5. Manually running pipelines](#75-manually-running-pipelines)
  - [8. The Pipeline step-by-step](#8-the-pipeline-step-by-step)
    - [8.1. Stage 1: Prepare](#81-stage-1-prepare)
      - [8.1.1. cleanup](#811-cleanup)
      - [8.1.2. workspace](#812-workspace)
        - [8.1.2.1 Oniro](#8121-oniro)
        - [8.1.2.2 Leda](#8122-leda)
    - [8.2. Stage 2: Build](#82-stage-2-build)
      - [8.2.1. Oniro](#821-oniro)
      - [8.2.2. Leda](#822-leda)
    - [8.3. Stage 3: Collect](#83-stage-3-collect)
      - [8.3.1. Tinfoilhat](#831-tinfoilhat)
      - [8.3.2. Aliensrc](#832-aliensrc)
    - [8.4. Stage 4: Analyze - Aliens4Friends](#84-stage-4-analyze---aliens4friends)
    - [8.5. Stage 5 (optional): SBOM](#85-stage-5-optional-sbom)
  - [9. Source Code Repositories](#9-source-code-repositories)

<!-- /TOC -->

## 1. Introduction: Context, Analysis and Design of the Pipelines

This repo contains pipeline definitions to implement a Continuous Compliance
workflow for repositories of Yocto-based, multi-target operating system projects
like Oniro. The goal is to continuously scan (through ScanCode and Fossology)
the source code of all first party and third party components that are included
in Oniro reference builds for multiple possible targets, so that the scan
results can be continuously validated by the Audit Team, and potential IP issues
can be spotted and solved early in the development process.

The peculiar nature of the Oniro project (and of any other Yocto-based,
multi-target project) has relevant implications for the continuous compliance
process which is being automated through CI/CD pipelines; we will briefly recap
here the most relevant aspects, in order to better understand the rationale
behind the pipeline design (better described in this [analysis
document](./doc/20210825_initial_analysis.md)).

### 1.1. Package and metadata aggregation

Yocto has not a package manager but is based on stratified layers of build
recipes that may interfere with each other (overrides etc.) so the final result
(target image) depends on a specific combination of layers and configuration
parameters and can be analyzed only *during* or *after* build.

Yocto can create source and binary packages in various formats, but only
*post-mortem* (i.e. after build, just for software composition analysis
purposes) and only specific to a particular built image.

Different targets in the build matrix may partly share the same software
components, but while the upstream source of such components would the same,
usually downstream modifications applied by Yocto recipes may be different for
each different target.

If we used Yocto functionality to create source packages to analyze, it would
create a different package for each different target even if the upstream
component is the same across different targets, and we would end up with a
package proliferation that would be hardly manageable for the Audit Team which
has to review source packages in Fossology.

Therefore we need to aggregate sources and source metadata from all possible
build targets in the build matrix to create source packages that aggregate all
possible source variants for different targets in the same project release or
snapshot.

To do that, we need to build all possible targets in the build matrix for a
given release or snapshot, and scan all the build directories with [TinfoilHat]
in order to collect metadata and create "aggregated" source packages to be
uploaded and reviewed on Fossology.

All such "aggregated" source packages are then analyzed and processed through
[Aliens4Friends], which finally uploads them to Fossology for being reviewed by
the Audit Team, and also collects stats data from Fossology to publish them in a
dedicated [Dashboard].

[TinfoilHat]: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/tinfoilhat

[Aliens4Friends]: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends

[Dashboard]: https://github.com/noi-techpark/aliens4friends-dashboard


### 1.2. Pipeline triggers

Given the structure of a Yocto-based project, the final build results across the
whole target matrix may be affected:

  a) by a change in the source code component integrated by a recipe

  b) by a change in the recipe/layer that integrates it

  c) by a change in another layer that overrides that recipe

  d) by a change in the manifest that adds or removes (or changes the revision
  of) a certain layer

Thus compliance pipelines (as build pipelines) should ideally run against every
commit and merge request to the main branch(es):

  1. in the manifest repo,

  2. in all first party layer repos (even if, as to Oniro, most of them have
  been included in the same repo as the manifest, so cases 1 and 2 mostly
  coincide), and

  1. in all first party source repos.

In case 2 (layer changes), if the changed layer stays in a different repo from
the manifest, the pipeline would need to fetch the manifest and the layers, and
then checkout the cloned git repo of that specific layer that has changed to the
commit that needs to be tested, and only then fetch source repos and start the
build.

In case 3 (source component changes), the pipeline would need to fetch the
manifest and the layers, and then use a dedicated tool provided by Yocto
(devtool) to override the specific recipe which integrates the changed
component, so that it points to the component's source revision that needs to be
tested.

### 1.3. Pipeline types and run frequency

In previous implementations of the compliance pipelines, we used to trigger
three different pipelines with different frequencies: the "complete" pipeline
was scheduled only periodically (usually weekly) while a "partial" pipeline
running only on a filtered set of components (particularly, components that did
not have a good match, or were not found at all, in the Debian distribution -
i.e., "non-friend" or "alien" packages) was triggered by every commit; finally,
a third pipeline, aimed at just updating the audit progress data for the
dashboard, was run nightly.

Such differentiation was due both to the extremely long execution time of the
"complete" pipeline and to the need to balance the audit team workload over
time. However, as for the first aspect, some optimization tricks have been
implemented in order to speed up execution time (parallel execution of some
jobs, better use of bitbake cache to build the target matrix); as for the second
aspect, a different strategy has been found and implemented for the audit team
workflow, based on a "risk analysis first" approach, providing that a quick risk
analysis based on some rules of thumb is immediately performed on any new
component (or set of new components) uploaded by compliance pipeline, in order
to immediately spot possible critical legal issues, while in-depth review and
analysis is postponed to a later stage.

In the current implementation, only one pipeline type, that builds and scans the
latest project snapshot, is triggered by every commit in the main branch of the
oniro repo. A new solution to ease the work of the Audit Team is to put incoming
package variants (i.e. mere variants of packages already uploaded and being
reviewed in Fossology, where just some more downstream patches are added to the
same upstream package) into a separate Fossology folder.

Based on audit team needs from time to time, and on future project
developments, multiple pipelines might be implemented again in the future.

## 2. Docker images

To run compliance pipelines, a couple of dedicated docker images are needed.

1. The first one is available at
   `docker.io/onirocompliancetoolchain/pipelines:latest` and is built from this
   repo; it basically adds [TinfoilHat] scripts (from the tinfoilhat repo,
   included here as a submodule) and some bash scripts needed by compliance
   pipelines (found in `scripts/` folder) to Oniro's "official" bitbake-builder
   image; it is intended to be used to run the first part of the pipeline, that
   builds the whole target matrix and then collects metadata and create
   aggregate software packages through [TinfoilHat].

2. The second image is available at
   `docker.io/onirocompliancetoolchain/aliens4friends-toolchain:latest` and it
   is built from the [Aliens4Friends] repo, and it is intended to be used to run
   the second part of the pipeline, that process aggregate packages and uploads
   them to Fossology.

For other projects than Oniro, image 1 above needs to be replaced by a dedicated
docker image, built from the base docker image needed to build the project, plus
the required scripts. A working example for [Eclipse
Leda](https://eclipse-leda.github.io/leda/) can be found at
`docker.io/onirocompliancetoolchain/pipelines_leda:latest`.

## 3. External bash scripts

In order to allow readability, reusability and modularity of pipeline
definitions, all the logic has been moved to external bash scripts placed in
`scripts/` folder, which are also included in the docker image mentioned above.

### 3.1. Oniro scripts

**`oniro-workspace.sh`** creates the workspace needed by bitbake to build the
target matrix for Oniro, by fetching the project manifest and all required
bitbake layers through the `repo` command; it requires some environment
variables:

- `CI_PROJECT_NAME` and `CI_PROJECT_URL` come from Gitlab CI environment and
  do not need to be manually set

- `PROJECT`: the project name (used to name the parent folder of the workspace
  directory)

- `BUILD_DIR`: build parent folder, like `/build` (it should be permanent)

- `ALIEN_TEMP_DIR`: temporary directory (it does not need to be permanent but it
  should have enough disk space)

- `MANIFEST_URL`, `MANIFEST_NAME`, `MANIFEST_BRANCH` (and optional:
  `MANIFEST_MIRROR`):  parameters for `repo` command

- `FORCE_RELEASE`: optional, may be used to re-run a pipeline on a project
  release/snapshot that had been already processed by a previous pipeline

**`yoctobuild.sh`** creates a bitbake build dir and runs bitbake on a series of
recipes/target images. It requires the following environment variables:

- `WORKDIR`: workdir where to create the bitbake build dir (usually it should be
  the directory containing yocto layers and bitbake itself); please note that
  **this variable is automatically set** by `oniro-workspace.sh` to
  `$BUILD_DIR/$PROJECT/$RELEASE` so it does not need to be manually set

- `MACHINE`: target machine; it will be saved in `conf/local.conf` in the
  build dir

- `BUILD_TAG`: arbitrary, it will be used together with `MACHINE` to name the
  build dir (like in `build-$BUILD_TAG-$MACHINE`)

- `TEMPLATECONF`: bitbake template configuration path (**relative to the build
  dir**, so it should usually start with `../`)

- `OE_INIT_BUILD_ENV`: oe-init path (**relative to `WORKDIR`**)

- `RECIPES`: sting containing recipes to build, separated by spaces or
  linebreaks

- `SKIP_FAILED_BUILDS`: optional; if set, does not retry to build failed
  builds when running a pipeline again against an already processed commit

- `FORCE_REBUILD`: optional; if set, removes all `.lock` and `.sock` files in
  the build dir (it may be useful when the build in a previous pipeline on the
  same commit has been ungracefully killed)

- `*BB_LOCAL_CONF`: given that different build machines, different hardware
  targets, different contexts, etc. may require different local configuration
  variables for bitbake, one may set such variables at any point of the
  pipeline definition (including within the parallel job matrix) by adding a
  multiline string variable whose name is or ends with `BB_LOCAL_CONF` --
  keeping in mind that variables with the same name may override one another
  (please refer to Gitlab CI documentation), so it is better to use different
  names all ending with `*BB_LOCAL_CONF`, unless one does need to override
  (eg. in the build matrix). Example:

```yaml
COMMON_BB_LOCAL_CONF: |
  CONNECTIVITY_CHECK_URIS = "https://example.net/"
  IMAGE_VERSION_SUFFIX = ""
  CVE_CHECK_DB_DIR = "$${TMPDIR}/CVE_CHECK/"
  INHERIT += "cve-check"
  USER_CLASSES += "buildstats buildstats-summary"
  BB_GENERATE_MIRROR_TARBALLS = "1"
  INHERIT += "rm_work"
```

**Please note the double `$$` before the TMPDIR variable**, required to prevent
bash from replacing it -- it will become a single `$` in `conf/local.conf`.

**`yoctobuild-cleanup.sh`** deletes build dirs older than N days within
workspace dirs, excluding release build dirs (alpha, beta, rc, etc) which are
kept anyway. Also the other contents of workspace dirs (eg. yocto layers, etc.)
are kept, for debugging/reproducibility. Usage: `yoctobuild-cleanup.sh
BASEPATH BUILD_CLEANUP_DAYS [dryrun|delete]`

### 3.2. Leda scripts

**`leda-workspace.sh`** creates the workspace needed by bitbake to build the
target matrix for Leda, by patching the relevant [kas] configuration files and
running the `kas checkout` command for each target machine. It requires some
environment variables:

- `PROJECT`: the project name (used to name the parent folder of the workspace
  directory)

- `BUILD_DIR`: build parent folder, like `/build` (it should be permanent)

- `DL_DIR` and `SSTATE_DIR`: Yocto downloads and sstate-cache directories (they
  should be permanent)

There is no specific build script for Leda, because once workspace has been set
up, each machine may be built with a simple `kas build` command, that can be
easily implemented in the pipeline definition. <!--TODO build script for leda-->

As to **`yoctobuild-cleanup.sh`**, see section [3.1](#31-oniro-scripts) above.

## 4. Modular pipeline definition: yaml files

In order to allow reusability, pipeline definitions have been split in modular
files that can be combined together.

In include order, we need:

1. a yaml file defining the docker image needed to build the project and that
   includes also the necessary scripts (see section [2](#2-docker-images)
   above) as well as rule definitions to run the pipeline only on specific
   branches (**project-specific**);

2. a yaml file defining variables specific to the gitlab runner used to build
   and scan the project (**project-specific** and **infrastructure-specific**);

3. a common yaml file defining all pipeline jobs (**general**);

4. a yaml file defining project-specific variables and build jobs, as well as
   the build matrix (**project-specific**).

Finally, other variables need to be set directly in Gitlab CI settings (see
section [6.2](#62-pipeline) below).

### 4.1. Oniro

1. **`oniro-branch-rules.yml`**: defines the docker build image and provides
   that pipelines may be run only against `kirkstone` and `staticrelease-v2.0.0`
   branches

2. **`ci-compliance-runner.yml`** contains tags and variables specific to the
   gitlab runner used to build and scan Oniro:

   - `BUILD_DIR`, `ALIEN_TEMP_DIR`: see `oniro-workspace.sh`
     [above](#3-external-bash-scripts)

   - `BUILD_CLEANUP_DAYS`: parameter for `yoctobuild-cleanup.sh` (see
     [above](#3-external-bash-scripts))

   - `A4F_POOL`: Aliens4Friends data pool directory path (it must be permanent)

   - `RUNNER_BB_LOCAL_CONF`: see `yoctobuild.sh`
     [above](#3-external-bash-scripts)

3. **`ci-default.yml`**: common yaml file, see above.

4. **`oniro.yml`** is the file that intended to be actually used as CI pipeline
definition for Oniro: it includes the previous two files, it adds some specific
variables, and it leverages [Gitlab CI parallel job matrix] functionality (which
allows to "run a job multiple times in parallel in a single pipeline, but with
different variable values for each instance of the job") to build the whole
target matrix. The following general variables need to be defined in this kind
of file:

   - `PROJECT`, `MANIFEST_URL`, `MANIFEST_NAME`, `MANIFEST_BRANCH`: see
     `oniro-workspace.sh` [above](#3-external-bash-scripts)

   - `BITBAKELIB_RELPATH` and `OE_LIB_RELPATH`: bitbake and oe-core library
     paths, relative to `WORKDIR` (eg. `bitbake/lib`), needed to run
     [TinfoilHat]

   - `FOSSY_FOLDER`: Fossology folder where to upload packages

   - `FOSSY_FALLBACK_FOLDER`: Fossology folder where to upload variants of
     already uploaded packages (see the end of section
     [1.3](#13-pipeline-types-and-run-frequency) above)

   - `WORKSPACE_SCRIPT`: script used to create workspace; in `oniro.yml` is set
     to `oniro-workspace.sh` (see [above](#3-external-bash-scripts)), but it may
     be set differently in other contexts

   - `TEMPLATECONF`, `*BB_LOCAL_CONF` (where needed), `OE_INIT_BUILD_ENV`: see
     `yoctobuild.sh` [above](#3-external-bash-scripts)

[Gitlab CI parallel job matrix]: https://docs.gitlab.com/ee/ci/yaml/#parallelmatrix

Furthermore, the following variables need to be defined within the build matrix
definition:

- `MACHINE`, `RECIPES`, `BB_LOCAL_CONF`: see `yoctobuild.sh`
  [above](#3-external-bash-scripts)

- other arbitrary variables for convenience/better redability (eg. in
  `oniro.yml` a `FLAVOUR` variable is set to define both `TEMPLATECONF` and
  `BUILD_TAG` to "visually" reflect Oniro "flavours" -- linux, zephyr,
  freertos)

### 4.2. Leda

1. **`leda-branch-rules.yml`**: defines the docker build image and provides that
   pipelines may be run only against the `main` branch

2. **`ci-compliance-runner_leda.yml`** contains tags and variables specific to
   the gitlab runner used to build and scan Leda:

   - `BUILD_DIR`, `ALIEN_TEMP_DIR`: see [above](#3-external-bash-scripts)

   - `BUILD_CLEANUP_DAYS`: parameter for `yoctobuild-cleanup.sh` (see
     [above](#3-external-bash-scripts))

   - `A4F_POOL`: Aliens4Friends data pool directory path (it must be permanent)

   - `DL_DIR` and `SSTATE_DIR`: see `leda-workspace.sh`
     [above](#external-bash-scripts)

3. **`ci-default.yml`**: common yaml file, see section [4.1](#41-oniro) above.

4. **`leda.yml`** is the file that intended to be actually used as CI pipeline
definition for Oniro: it includes the previous two files, it adds some specific
variables, and it leverages [Gitlab CI parallel job matrix] functionality (which
allows to "run a job multiple times in parallel in a single pipeline, but with
different variable values for each instance of the job") to build the whole
target matrix. The following general variables need to be defined in this kind
of file:

   - `PROJECT`: see `leda-workspace.sh` [above](#3-external-bash-scripts)

   - `BITBAKELIB_RELPATH` and `OE_LIB_RELPATH`: bitbake and oe-core library
     paths, relative to `WORKDIR` (eg. `bitbake/lib`), needed to run
     [TinfoilHat]

   - `FOSSY_FOLDER`: Fossology folder where to upload packages

   - `FOSSY_FALLBACK_FOLDER`: Fossology folder where to upload variants of
     already uploaded packages (see the end of section
     [1.3](#13-pipeline-types-and-run-frequency)" above)

   - `WORKSPACE_SCRIPT`: script used to create workspace; in `leda.yml` is set
     to `leda-workspace.sh` (see [above](#3-external-bash-scripts)), but it may
     be set differently in other contexts

Furthermore, the `MACHINE` variable list need to be defined within the build
matrix definition.

## 5. Compliance Pipelines and Repo Mirroring

The pipeline defined in this repo is intended to be run in a GitLab mirror of
the original repository to scan, leveraging the repository mirroring
functionality of GitLab, which is expressly intended to be used also to this
purpose. Such functionality allows to have an external repo mirrored in real
time within GitLab, and to get GitLab pipelines triggered on the mirrored repo
at every commit of the original repo (for further details, see
'[GitLab CI/CD for external repositories][GL-ext]' in GL Docs).

[GL-ext]: https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/

The choice of this approach is due to a couple of reasons.

1. On the one hand, **IP Compliance Pipelines may take a very long time to
   complete** (in the worst case, hours) while on the other hand, **the
   artifacts they provide are not directly consumed by software developers** but
   by IP Compliance auditors -- so it is advisable to run such pipelines
   separate from test pipelines, lest the latter get pointlessly blocked by the
   former.

2. Not all project-related repositories (Yocto meta layers, source components)
   may be hosted on the same git server, while we need all pipelines to run on
   the same server.

## 6. Current implementation (updated 2023-04-03)

### 6.1. Mirrors

- **Oniro**: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/mirrors/oniro-goofy

- **Leda**: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/demo/leda

### 6.2. Pipeline

This pipeline uses [TinfoilHat] and [Aliens4Friends] to collect and aggregate
licensing and copyright information from a set of different Oniro builds for
different build targets and images, and provide output for the IP Compliance
[Dashboard].

In the Gilab mirror repos (eg. the [main Oniro repository]), the  `Settings >
CI/CD > General pipelines > CI/CD configuration file` parameter is set to
`https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/pipelines/-/raw/master/oniro.yml`,
in order to point to the `oniro.yml` contained in this repo's main branch. If
you want to use another branch like `development` put a direct link to the file,
as for example:
`https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/pipelines/-/raw/development/oniro.yml`.

> **NOTE** For Leda, an alternative scheme is being used in CI/CD settings to
> point to the right yaml file (it should be equivalent to the previous one):
> `leda.yml@eclipse/oniro-compliancetoolchain/toolchain/pipelines:master`.

to enable connection with Fossology, you should set some required variables
inside your GitLab, either as group variables or in your project. Go to
`Settings > CI/CD > Variables` and add the following variables:

- `FOSSY_GROUP_ID`: usually, `3` (the 'fossy' group id)

- `FOSSY_USER`: usually, `fossy`

- `FOSSY_PASSWORD`

- `FOSSY_SERVER`: Fossology's full url, like in
  https://my-fossology-instance/repo

- `FOSSY_HOSTNAME`: local hostname, like `my-fossology-instance`

- `FOSSY_SSL_CERT`: Fossology server's apache2 pem certificate in x509 format (usually found at `/etc/ssl/certs/ssl-cert-snakeoil.pem`, if self-signed)

- `FOSSY_IP_ADDRESS`: Fossology IP address

## 7. GitLab Setup

To setup these pipelines, we need to setup a server and install a gitlab runner
on it, and finally mirror repositories we want to scan and configure their CI/CD
pipelines.

To do that, we need to use the "Run CI/CD for external repository" available in
GitLab EE, as explained above. In this way, compliance pipelines (which may take
a long time to run) can be temporarily run in a separate environment without
interfering with other pipelines, until they are ready to be integrated.

Generic installation instructions can be found inside
[Aliens4Friends/README.md](https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends#gitlab-ci-of-a-complete-pipeline-with-yocto-and-aliens4friends).

Please keep in mind that the pipelines, in order to work, need a (large)
permanent storage for:

- the Aliens4Friends data pool, containing collected metadata used to produce
  the SBOM and to fill the dashboard

- Yocto build dirs, which need to be kept in order to collect metadata from them
  with TinfoilHat, and also to allow to resume failed pipelines when some build
  or scan error happens; the content of these dirs is deleted after some time
  (currently 5 days), apart from bitbake layers and metadata, which are kept for
  a longer period of time (TBD), to allow audit reproducibility over time

- Yocto download and SSTATE cache, which is needed to speed up build time (also
  considering that a large build matrix need to be built every time the pipeline
  runs).

The specific details of the current production environment are as follows.

### 7.1. Server

#### 7.1.1. Oniro

  - OS: Ubuntu 20.04 x86_64
  - Memory: 32 GB RAM (no swap)
  - CPU: AMD EPYC 7R13 Processor @ 2.60GHz (16 cores)
  - mount points:
    - `/` (ext4, 60GB)
    - `/a4fpool` (ext4, 500GB)
    - `/build` (ext4, 6TB)
  - software:
    - Docker Daemon: v20.10.20
    - GitLab Runner: v15.4.0

`/a4fpool` contains the Aliens4Friends data pool.

`/build` contains both a temporary dir used by TinfoilHat and the shared
download and SSTATE cache dirs, used by all pipelines:

```
/build/common/downloads
/build/common/sstate-cache
/build/tmp
```

The above dirs need to be manually created when setting up the server.

Both `/a4fpool` and `/build` must be owned by user with uid 1000 (which is the
user that will run all commands within GL runner docker containers). With ext4
filesystem, this requires a permanent workaround in `/etc/rc.local`:

```bash
#!/bin/bash

chown ubuntu: /build
chown ubuntu: /a4fpool
```

("ubuntu" is the user name with uid 1000 on our system; `/etc/rc.local` is made executable, to run at boot time).

#### 7.1.2. Leda

The gitlab runner server for Leda has pretty much the same characteristics; the
only differences are the Ubuntu version (22.04, to match the Ubuntu version of
the builder docker image) and the storage (1.5T for `/build`, since Leda build
matrix is smaller and requires less disk space).

### 7.2. GitLab Runner Configuration

Docker was installed by following its [official
instructions](https://docs.docker.com/engine/install/ubuntu/).

The gitlab runner server was created and registered by following the
instructions from our gitlab instance (the gitlab-runner version shipped with
Ubuntu is outdated and would not work). When registering the runner, executor
must be set to "docker", while runner tags should correspond to the ones set in
the applicable `*-branch-rules.yaml` file (see Section
[4](#4-modular-pipeline-definition-yaml-files) above).

After registration, the configuration file at `/etc/gitlab-runner/config.toml` needs to be changed as follows:

```diff
-concurrent = 1
+concurrent = 2
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "<runner-name>"
  url = "https://gitlab.eclipse.org/"
  id = <id>
  token = "<token>"
  token_obtained_at = <date>
  token_expires_at = <date>
  executor = "docker"
+  output_limit = 102400
  [runners.cache]
    MaxUploadedArchiveSize = 0
  [runners.docker]
+    device_cgroup_rules = []
+    services_privileged = false
+    devices = []
+    pull_policy = ["always"]
+    memory = "28G"
    tls_verify = false
    image = "debian:10"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
-    volumes = ["/cache"]
+    volumes = ["/cache", "/a4fpool:/a4fpool:rw", "/build:/build:rw"]
    shm_size = 0
+    [runners.docker.container_labels]
+    [runners.docker.services_tmpfs]
+    [runners.docker.tmpfs]
+    [runners.docker.sysctls]
```

The `concurrent` parameter value (i.e., the maximum number of concurrent jobs)
needs to be tested and adjusted in production. Depending on the average pipeline
workload, it may be set between 2 and 4.

The parameters under `[runners.docker]` and at the end need to be added to get
rid of [annoying
warnings](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/30844) in most
recent gitlab-runner versions.

The `volume` parameter needs to be changed in order to enable docker containers
to access permanent storage volumes (see [above](#71-server)).

The `memory` parameter should be set to a lower value than the total system
memory, in order to avoid out-of-memory errors and system hangings with
concurrent pipelines.

For the same reason, we created a SystemD slice for the memory:

```
# /etc/systemd/system/limit-docker-memory.slice
[Unit]
Description=Slice with MemoryLimit=28G for docker
Before=slices.target

[Slice]
MemoryAccounting=true
MemoryLimit=28G
```

`/etc/docker/daemon.json`:

```
{
    "cgroup-parent": "limit-docker-memory.slice"
}
```

After creating the above files, we reloaded systemctl and restarted docker:

```
systemctl daemon-reload
systemctl restart docker
```

Finally, for concurrent build jobs to work, **the following parameter must be
set**:

```shell
sysctl fs.inotify.max_user_watches=1000000
```

### 7.3. GitLab Badge to link results to the Aliens4Friends SCA dashboard

We assume that your Aliens4Friends SCA dashboard runs on
https://sca.software.bz.it/ and that you use https://gitlab.eclipse.org as
your GitLab instance. Please adapt accordingly, if you use a different setup.

Go to `Settings > General > Badges` and click on `Add badge`:

```
Name            : Latest
Link            : https://sca.software.bz.it/?json=https://gitlab.eclipse.org/%{project_path}/-/jobs/artifacts/%{default_branch}/raw/report.harvest.json?job=harvest
Badge image URL : https://gitlab.eclipse.org/%{project_path}/-/jobs/artifacts/%{default_branch}/raw/dashboard-badge.svg?job=harvest
```

In our current implementation, we added also another badge pointing to the
results related to the latest release (and not to the latest commit),
by substituting `%{default_branch}` with the release branch name.

### 7.4. Known limitation: Single branches only

The logic to implement pipelines for merge requests, external layers/repos, and
multibranch support (see [above](#12-pipeline-triggers)) has been included in
`oniro-workspace.sh` script, but it has been commented out since it needs
additional testing. We will activate it at a later stage if it will be needed.

### 7.5. Manually running pipelines

We implemented some environmental variables to override default execution
parameters, which can be set inside GitLab when you click `CI/CD > Pipelines >
Run pipeline` (see also  `yoctobuild.sh` [above](#3-external-bash-scripts)):

- `SKIP_FAILED_BUILDS`: `<any value>`. When running a pipeline against a project
  snapshot that has already been built by a previous pipeline (thus using the
  same build dir in the permanent storage) do not try to re-build build targets
  that already failed in the previous pipeline.

- `FORCE_RELEASE`: `<release of a Yocto manifest>`. This is only possible in a
  second run, when it has already been prepared in a previous pipeline. The
  reason for this is just to resume a stopped pipeline, when we've got already a
  new release of the manifest repo.

- `FORCE_REBUILD`: if set, removes all `.lock` and `.sock` files in the build
  dir (it may be useful when the build in a previous pipeline on the same commit
  has been ungracefully killed)

- `CI_GENERATE_SBOM`: if set, SPDX SBOM generation jobs are added at the end of
  the pipeline (see "SBOM" stage at section [8.5](#85-stage-5-optional-sbom)
  below); since they may take a very long time to complete, and SBOM generation
  is not always needed, we left them optional

The **first three variables** are implemented **only for Oniro**, for now.
<!-- TODO add them also in Leda build scripts -->

## 8. The Pipeline step-by-step

The schema of the workflow run by the pipelines is as follows:

![](img/aliens4friends-schema-2021-12-17.png)

We describe the whole process of the compliance pipeline here.
Lets start to explain the different stages:

- [Stage 1: Prepare](#81-stage-1-prepare)
- [Stage 2: Build](#82-stage-2-build)
- [Stage 3: Collect](#83-stage-3-collect)
- [Stage 4: Analyze - Aliens4Friends](#84-stage-4-analyze---aliens4friends)
- [Stage 5 (optional): SBOM](#85-stage-5-optional-sbom)

Each job creates `.success-[job-name]` or `.failure-[job-name]` files in the job
dir.[^success-failure] We can skip tasks in future runs or debug already ran
pipelines by looking at those files, which are saved in a permanent storage
(i.e. the yocto build dir within the `/build` mount point).

[^success-failure]: this is done directly by the bash scripts in `scripts/` for
the first part of the pipeline, and by a temporary wrapper script created during
pipeline execution for the second part of the pipeline running `aliens4friends`.
The latter solution is a little bit hacky and should be changed in the future,
by integrating the wrapper script in aliens4friends.

The `yoctobuild.sh` script (used in the Build stage) also produces a
`.log-yoctobuild-[target]` file for each build target/image, to ease local log
inspection in case of build errors. <!--TODO add build logs also for leda-->

### 8.1. Stage 1: Prepare

#### 8.1.1. cleanup

Clean old runs. `yoctobuild-cleanup.sh` deletes all build dirs that
are old than *n* days. See [above](#3-external-bash-scripts) for details.

#### 8.1.2. workspace

##### 8.1.2.1 Oniro

First, we fetch the Yocto manifest repo and extract the current RELEASE from it.

Second, we add additional information to that RELEASE string. Those information
describe whether the current repository is the manifest repo itself or another
repository or commit (currently only the first option - scan manifest repo - is
implemented, and the other options need to be tested yet). At the end RELEASE
contains the manifest tag/commit, and - when scan of external layer repos will
be implemented - the name and commit of the external repo that will override the
corresponding one in the manifest.

Third, we create a .env file, which contains all necessary information for all
coming stages: `PROJECT`, `RELEASE` and `WORKDIR`

##### 8.1.2.2 Leda

Since Leda is built through [kas], workspace is created by just adjusting the
required parameters in kas configuration files, and by running `kas checkout`.

### 8.2. Stage 2: Build

#### 8.2.1. Oniro

This stage builds all possible combinations of Oniro "flavours", machines and target images. It fails if one combo exits with an error, however we could make this stage permissive by setting `allow_failure` to `true` in the pipeline definition, because during alpha development some builds are expected to fail at times, and this should not stop compliance pipelines; however **such setting should be removed after beta release** because failed builds cannot be parsed by [TinfoilHat] and therefore relevant packages may be missing and not reviewed by the audit team.

The build combinations are defined by using the [Gitlab CI parallel job matrix] functionality, which allows to "run a job multiple times in parallel in a single pipeline, but with different variable values for each instance of the job" -- in this specific case, with different values for `FLAVOUR`, `MACHINE`, `RECIPES` and `BB_LOCAL_CONF` variables.

In this way, the resulting matrix data looks very simple and straightforward:

```yaml
parallel:
  matrix:
    - FLAVOUR: linux
      MACHINE: [qemux86-64, qemux86, raspberrypi4-64, seco-intel-b68]
      RECIPES: |
        oniro-image-base
        oniro-image-base-dev
        oniro-image-base-tests
        oniro-image-extra
        oniro-image-extra-dev
        oniro-image-extra-tests
    - FLAVOUR: linux
      MACHINE: [seco-imx8mm-c61-2gb, seco-imx8mm-c61-4gb]
      RECIPES: |
        oniro-image-base
        oniro-image-base-dev
        oniro-image-base-tests
        oniro-image-extra
        oniro-image-extra-dev
        oniro-image-extra-tests
      BB_LOCAL_CONF: |
        ACCEPT_FSL_EULA = "1"
    - FLAVOUR: zephyr
      MACHINE: [qemu-x86, qemu-cortex-m3, 96b-avenger96, 96b-nitrogen, nrf52840dk-nrf52840, arduino-nano-33-ble]
      RECIPES: zephyr-philosophers
    - FLAVOUR: freertos
      MACHINE: qemuarmv5
      RECIPES: freertos-demo
      BB_LOCAL_CONF: |
        CVE_CHECK_CREATE_MANIFEST += "0"
```


This stage may need several hours, depending on the delta that has changed from
the previous pipeline (components that did not change are not rebuilt and are
just taken from yocto SSTATE cache).

#### 8.2.2. Leda

Leda's build matrix is much simpler than Oniro's; currently there are only three different target machines. Moreover, bitbake configuration parameters are already handled by [kas], so it is not needed to set them in the pipeline.

### 8.3. Stage 3: Collect

#### 8.3.1. Tinfoilhat

Tinfoil is a [wrapper around BitBake's internal code][tinfoil] that can be used
to get the value that environment variables actually assume at build time.
[TinfoilHat] is a wrapper around Tinfoil, in order to do Software Composition
Analysis (SCA) on bitbake-based projects, by packaging software components and
reconstructing the Bill of Material (BoM).

In this stage we collect all relevant data from all builds performed in the
previous stage and store it inside a folder called `tinfoilhat` inside our
working dir.

#### 8.3.2. Aliensrc

This stage uses [aliensrc_creator]. We run Aliensrc Creator against bitbake
build dirs after build processes successfully ran, to create source packages
from those specific builds. It takes as input the `.tinfoilhat.json` files
created by TinfoilHat, as well as the downloaded source tarballs, the local
files and the git repos cloned within bitbake build dirs.

The output gets stored inside the folder `aliensrc` in our working dir.

This step aggregates sources and metadata from different build targets, so if
the same recipe/package is found in more than one build, but with some variants
(typically, the upstream source code is the same but downstream patches and
additions are different across different build targets), all the variants are
packaged in one single aliensrc package, in order to avoid confusion and package
proliferation.

### 8.4. Stage 4: Analyze - Aliens4Friends

This stage uses [aliens4friends] (a4f). The pipeline has to perform these jobs:

- **session-lock**: initialize and lock a4f session for the project
  release/snapshot to be analyzed, to avoid that multiple pipelines use the same session (see a4f documentation on [session locking])

- **add**: add tinfoilhat files and aliensrc packages created in the previous
  stage (see a4f documentation on [add command])

- **match** and **snapmatch**: find and download matching packages from Debian
  repositories, using standard debian API and debian snapshot API, and calculate
  a similarity score (see a4f documentation on [match and snapmatch commands])

- **scan**: we scan these packages with [Scancode] to detect license and
  copyright information, and store its results in the pool (see a4f
  documentation on [scan command])

- **delta**: compare Debian packages with aliensrc packages to find differences
  between Alien Packages and the corresponding Debian packages (by
  "differences", we mean the differences in terms of
  licensing/copyright/intellectual property, so we just care if license and
  copyright statements (if any) have changed, not if just code has changed; see
  a4f documentation on [delta command] for details)

- **spdxdebian** and **spdxalien**: create SPDX files for Debian and aliensrc
  packages and store them in our pool (see a4f documentation on [spdxdebian
  command] and on [spdxalien command] for details)

- **session-add-variants**: add package variants to the session for audit
  progress calculation consistency in the Dashboard (see a4f documentation on
  [--add-variants option])

- **upload**: upload packages to, and download results from, Fossology (see a4f
  documentation on [upload command] for details)

- **harvest**: harvest all information gathered during previous steps and create
  a final report, which contains all information needed by the SCA dashboard
  (see a4f documentation on [harvest command]); the final report is made
  available as a pipeline artifact

- **session-unlock**: finally unlock the session, in order to allow to run a new
  pipeline against the same session (see "session-lock" above)

- **fossy-manual-update**: this is a manual job, intended to be manually run by
  the Audit Team to update Fossology stats related to a specific project
  snapshot; after that, also the harvest job should be manually run again on the
  same pipeline (through the "retry" button)

### 8.5. Stage 5 (optional): SBOM

The "harvest" job described above collect stats on licenses, audit progress,
etc. and  analyzing its output through the Dashboard may be useful to do many
SCA activities.

To produce an SPDX SBOM of all source components, there is an optional stage
(that may be triggered by setting `CI_GENERATE_SBOM` when running a manual
pipeline).

Since not all the available targets in the build matrix may be released, it is
possible to set a separate matrix of targets for which SPDX SBOM will be
generated, by using regular expressions on tinfoilhat tags, omitting the
`<project>/<release>` part (that will be automatically added), and filtering
only the `<flavour>[-<toolchain>]/<machine>/<image>` part, by defining a
`generate-sbom` job in this way (example):

```yaml
generate-sbom:
  extends: .generate-sbom
  parallel:
    matrix:
      - TARGET_DIR: linux-qemu
        RELEASE_REGEX: oniro-linux-(gcc|clang)/qemu(x86-64|x86|arm-efi|arm64-efi)/oniro-image-base
      - TARGET_DIR: linux-raspberrypi4
        RELEASE_REGEX: oniro-linux-(gcc|clang)/raspberrypi4-64/oniro-image-base
      - TARGET_DIR: zephyr-qemu
        RELEASE_REGEX: zephyr-gcc/qemu(-x86|-cortex-m3)/zephyr-philosophers
      - TARGET_DIR: zephyr-arduino-nano-33-ble
        RELEASE_REGEX: zephyr-gcc/arduino-nano-33-ble/zephyr-philosophers
```

The `TARGET_DIR` is the directory where SBOM artifacts will be saved in the pipeline workdir, so that they can be published as job artifacts.

[session locking]: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends#locking

[add command]: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends#step-4-add-the-alien-to-the-pool

[match and snapmatch commands]: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends#step-5-find-a-matching-debian-source-package

[scan command]: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends#step-6-scan-the-code-to-detect-licensecopyright-information

[delta command]: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends#step-7-find-differences-between-alien-packages-and-the-corresponding-debian-matching-packages

[spdxdebian command]: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends#step-8-create-debian-spdx-file-from-debiancopyright-file

[spdxalien command]: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends#step-9-create-alien-spdx-file-out-of-debian-spdx-file-reusing-license-metadata

[--add-variants option]: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends#adding-variants

[upload command]: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends#step-10-upload-to-fossology-schedule-fossology-scanners-import-aliendebian-spdx-to-fossology

[harvest command]: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends#step-12-harvest-all-results-and-create-a-final-report


## 9. Source Code Repositories

- Aliens4Friends: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends
- Aliens4Friends SCA Dashboard: https://github.com/noi-techpark/aliens4friends-dashboard
- TinfoilHat and Alienscr Creator: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/tinfoilhat
- Pipelines (this repo): https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/pipelines


<!-- URL aliases to not clutter paragraphs above -->
[OSTC toolchain repos]: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/
[Aliens4Friends/.gitlab-ci.yml]: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends/-/blob/master/.gitlab-ci.yml
[NOI Techpark Premium GitLab Account]: https://gitlab.com/noi-techpark-premium
[Aliens4Friends/utils]: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends/-/blob/master/infrastructure/utils/
[tinfoilhat]: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/tinfoilhat
[tinfoil]: https://wiki.yoctoproject.org/wiki/TipsAndTricks/Tinfoil
[aliensrc_creator]: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/tinfoilhat#aliensrc-creator
[aliens4friends]: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends#aliens-for-friends
[aliens4friends/session]: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends#step-3-create-a-session
[Scancode]: https://github.com/nexB/scancode-toolkit
[kas]: https://kas.readthedocs.io
