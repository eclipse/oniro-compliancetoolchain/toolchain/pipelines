Trademark Notice

Oniro and the Oniro Logo are trademarks of Eclipse Foundation. Other trademarks
and logos contained in this repo are owned by their respective registrant. All
such trademarks and logos are subject to the then-current trademark policies of
their respective owners and, if taken in isolation, are not licensed under the
license of the containing copyrighted matter.
