#!/bin/bash

# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>

req_vars="
BUILD_DIR
PROJECT
DL_DIR
SSTATE_DIR
"

missing=""
for v in $req_vars; do
  if [ -z "${!v}" ]; then
    echo "CI ERROR: required env variable $v is not defined!"
    missing="y"
  fi
done
if [ -n "$missing" ]; then
  echo "aborting"
  exit 1
fi

if [ -z "$1" ]; then
  echo "CI: missing env filename argument"
  exit 1
fi

env_file="$1"

set -e

git config --global --add safe.directory '*'

RELEASE=$(git describe --tags --always)

mkdir -p /build/$PROJECT/$RELEASE
rsync -ar ./ /build/$PROJECT/$RELEASE
curdir=$PWD
cd /build/$PROJECT/$RELEASE

python3 <<EOT
import yaml

conf_file = "kas/common-kirkstone.yaml"

with open(conf_file) as f:
     y = yaml.load(f, Loader=yaml.SafeLoader)

conf = y["local_conf_header"]["meta-leda"].split("\n")

conf.remove('INHERIT:remove = " cve-check"')
conf.append('INHERIT += "cve-check"')
conf.append('BB_GENERATE_MIRROR_TARBALLS = "1"')
conf.append('DL_DIR = "$DL_DIR"')
conf.append('SSTATE_DIR = "$SSTATE_DIR"')

y["local_conf_header"]["meta-leda"] = "\n".join(conf)

conf = y["local_conf_header"]["rauc-sign-conf"]
conf = conf.replace("\${TOPDIR}/../","", 2)
y["local_conf_header"]["rauc-sign-conf"] = conf

with open(conf_file, "w") as f:
     yaml.dump(y, f)
EOT

for f in kas/leda-*; do
  machine=${f/kas\/leda-/}
  machine=${machine/.yaml/}
  KAS_BUILD_DIR=$PWD/build-$machine kas checkout $f
done

ln -s $PWD/examples meta-leda/meta-leda-bsp/recipes-bsp/rauc/files/examples
# workaround for absolute URL (Issue #19)

echo "PROJECT=$PROJECT" >> "$env_file"
echo "RELEASE=$RELEASE" >> "$env_file"
echo "WORKDIR=$BUILD_DIR/$PROJECT/$RELEASE" >> "$env_file"

cd $curdir