#!/bin/bash

# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: NOI Techpark - Peter Moser <p.moser@noi.bz.it>
# SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>

set +e
set -o pipefail

vars="WORKDIR BUILD_TAG TEMPLATECONF OE_INIT_BUILD_ENV MACHINE RECIPES TOOLCHAIN"
missing=""

# optional vars:
# SKIP_FAILED_BUILDS FORCE_REBUILD

for v in $vars; do
  if [ -z "${!v}" ]; then
    echo "ERROR: required env variable $v is not defined!"
    missing="y"
  fi
done
if [ -n "$missing" ]; then
  echo "aborting"
  exit 1
fi

cd "$WORKDIR"
. "$OE_INIT_BUILD_ENV" "build-$BUILD_TAG-$MACHINE"

echo "----------------------------------------------------------------"
echo "checking for stuff to add to conf/local.conf"

confs=$(env | grep -ohe '^[^=]*BB_LOCAL_CONF=' | sed s/=//)

for conf in $confs; do
  if [ -n "${!conf}" ]; then
     local_conf="$(cat conf/local.conf)"
     already_added=$(echo $local_conf | grep "$(echo ${!conf})")
     # for this check to work reliably, *BB_LOCAL_CONF values must always
     # have more than one line (even by using an empty line at the beginning
     # if necessary)
     if [ -z "$already_added" ]; then
       echo "adding $conf content to conf/local.conf"
       echo "----------------------------------------------------------------"
       echo "${!conf}"
       echo "----------------------------------------------------------------"
       echo "${!conf}" >> conf/local.conf
     else
       echo "skipping $conf: already added"
       echo "----------------------------------------------------------------"
     fi
  fi
done

echo "checking for environment variables that need to be stored in"
echo "conf/local.conf (to be used by TinfoilHat later on)"
echo "----------------------------------------------------------------"

case $TOOLCHAIN in
  gcc)
    RUNTIME=gnu
    ;;
  clang)
    RUNTIME=llvm
    ;;
  *)
    echo "unknown TOOLCHAIN '$TOOLCHAIN'"
    exit 1
    ;;
esac

vars_to_store="MACHINE DISTRO TOOLCHAIN RUNTIME"

for v in $vars_to_store; do
  if [ -n "${!v}" ]; then
    to_store="$v = \"${!v}\""
    if ! grep "$to_store" conf/local.conf; then
      echo "$v = \"${!v}\""
      echo "$v = \"${!v}\""  >> conf/local.conf
    else
      echo "$v already stored, skipping"
    fi
  fi
done

echo "----------------------------------------------------------------"

[[ -n "$FORCE_REBUILD" ]] && rm *.lock *.sock 2>&1 >/dev/null

total=$(echo $RECIPES | wc -w)
count=0
failed_builds=""
successful_builds=""
for recipe in $RECIPES; do
  fulltag="$BUILD_TAG-$MACHINE-$recipe"
  success="$WORKDIR/.success-yoctobuild-$fulltag"
  failure="$WORKDIR/.failure-yoctobuild-$fulltag"
  logfile="$WORKDIR/.log-yoctobuild-$fulltag"
  count=$((count+1))
  echo "----------------------------------------------------------------"
  echo "building $BUILD_TAG-$MACHINE-$recipe... ($count/$total)"
  if [ -f "$success" ]; then
    echo "...already successfully built, skipping"
    successful_builds="$successful_builds$recipe "
    continue
  fi
  if [ -f "$failure" ]; then
    if [ -n "$SKIP_FAILED_BUILDS" ]; then
      echo "...build already failed before, skipping because SKIP_FAILED_BUILDS is set"
      failed_builds="$failed_builds$recipe "
      continue
    else
      rm "$failure"
    fi
  fi
  bitbake $recipe 2>&1 | tee "$logfile" && touch "$success" || touch "$failure"
  [[ -f "$failure" ]] && failed_builds="$failed_builds$recipe "
  [[ -f "$success" ]] && successful_builds="$successful_builds$recipe "
done

echo
if [ -z "$failed_builds" ]; then
  echo "----------------------------------------------------------------"
  echo "SUCCESS! built images: $successful_builds"
  exit 0
else
  echo "----------------------------------------------------------------"
  echo "I managed to build only: $successful_builds"
  echo "while the following images failed to build: $failed_builds"
  exit 1
fi
