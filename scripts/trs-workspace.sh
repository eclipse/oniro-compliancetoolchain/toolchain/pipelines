#!/bin/bash

# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>

req_vars="
BUILD_DIR
PROJECT
MANIFEST_URL
MANIFEST_NAME
MANIFEST_BRANCH
"

missing=""
for v in $req_vars; do
  if [ -z "${!v}" ]; then
    echo "CI ERROR: required env variable $v is not defined!"
    missing="y"
  fi
done
if [ -n "$missing" ]; then
  echo "aborting"
  exit 1
fi

if [ -z "$1" ]; then
  echo "CI: missing env filename argument"
  exit 1
fi

write_env () {
  echo "PROJECT=$PROJECT" >> "$env_file"
  echo "RELEASE=$RELEASE" >> "$env_file"
  echo "WORKDIR=$BUILD_DIR/$PROJECT/$RELEASE" >> "$env_file"
}

rm_temp () {
  cd "$curdir"
  rm -Rf "$tmpdir"
}

env_file="$1"

set -e

curdir=$PWD

git config --global --add safe.directory '*'
git config --global user.name builder
git config --global user.email builder@example.com

mkdir -p /build/$PROJECT/
tmpdir=$(mktemp -d -p /build/$PROJECT/)
cd $tmpdir

echo "----------------------------------------------------------------"
echo "CI: Building repo workspace with the following properties:"
echo "MANIFEST_URL: $MANIFEST_URL"
echo "MANIFEST_NAME: $MANIFEST_NAME"
echo "MANIFEST_BRANCH: $MANIFEST_BRANCH"
echo "----------------------------------------------------------------"

repo init -u $MANIFEST_URL -b $MANIFEST_BRANCH -m $MANIFEST_NAME
cd .repo/manifests
export RELEASE=$(git describe --tags --always)

if [ -f "$BUILD_DIR/$PROJECT/$RELEASE/.success-prepare" ]; then
  echo
  echo "CI: 'prepare' for $RELEASE succesfully run in a previous pipeline, skipping"
  rm_temp
  write_env
  exit 0
fi

cd ~
mv $tmpdir /build/$PROJECT/$RELEASE
cd /build/$PROJECT/$RELEASE
repo sync -j3

export DEBIAN_FRONTEND="noninteractive"
echo y | make apt-prereqs
make python-prereqs

touch .success-prepare
write_env
exit 0