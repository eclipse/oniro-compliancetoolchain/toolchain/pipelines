#!/bin/bash

# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: NOI Techpark - Peter Moser <p.moser@noi.bz.it>
# SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>

write_env () {
  echo "PROJECT=$PROJECT" >> "$env_file"
  echo "RELEASE=$RELEASE" >> "$env_file"
  echo "WORKDIR=$BUILD_DIR/$PROJECT/$RELEASE" >> "$env_file"
}

rm_temp () {
  cd "$curdir"
  rm -Rf "$tmpdir"
}

set -e

if [ -z "$1" ]; then
  echo "CI: missing env filename argument"
  exit 1
fi

env_file="$1"

req_vars="
CI_PROJECT_NAME
CI_PROJECT_URL
PROJECT
BUILD_DIR
ALIEN_TEMP_DIR
MANIFEST_URL
MANIFEST_NAME
MANIFEST_BRANCH
"

# optional vars: MANIFEST_MIRROR FORCE_RELEASE

missing=""
for v in $req_vars; do
  if [ -z "${!v}" ]; then
    echo "CI ERROR: required env variable $v is not defined!"
    missing="y"
  fi
done
if [ -n "$missing" ]; then
  echo "aborting"
  exit 1
fi

if [ -n "$FORCE_RELEASE" ]; then
  RELEASE="$FORCE_RELEASE"
  if [ ! -f "$BUILD_DIR/$PROJECT/$RELEASE/.success-prepare" ]; then
    echo "CI: ERROR: you cannot force release '$RELEASE' because it has not been already prepared in a previous pipeline"
    exit 1
  else
    echo "CI: forced release '$RELEASE' has been already prepared in a previous pipeline, skipping"
    write_env
    exit 0
  fi
fi

curdir="$PWD"
tmpdir=$(mktemp -d -p "$ALIEN_TEMP_DIR")
cd "$tmpdir"

echo "----------------------------------------------------------------"
echo "CI: Building repo workspace with the following properties:"
echo "MANIFEST_URL: $MANIFEST_URL"
echo "MANIFEST_NAME: $MANIFEST_NAME"
echo "MANIFEST_BRANCH: $MANIFEST_BRANCH"
echo "----------------------------------------------------------------"
[[ -n "$MANIFEST_MIRROR" ]] && reference="--reference $MANIFEST_MIRROR"
set -x
repo init \
  $reference \
  --manifest-url "$MANIFEST_URL" \
  --manifest-name "$MANIFEST_NAME" \
  --manifest-branch "$MANIFEST_BRANCH"
set +x

cd .repo/manifests

RELEASE="$(git describe --tags --always)"

# FIXME compare the following part with the new build pipeline
#if [ "$CI_PROJECT_URL" != "$MANIFEST_URL" ]; then
#  export RELEASE=$RELEASE"_$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA"
#  # RELEASE contains the manifest tag/commit, and the name and commit of
#  # the external repo that will override the corresponding one in the manifest
#fi

if [ -z "$RELEASE" ]; then
  echo "CI: ERROR: something's gone wrong, can't get RELEASE"
  rm_temp
  exit 1
fi

if [ -f "$BUILD_DIR/$PROJECT/$RELEASE/.success-prepare" ]; then
  echo
  echo "CI: 'prepare' for $RELEASE succesfully run in a previous pipeline, skipping"
  rm_temp
  write_env
  exit 0
fi

if [ -d "$BUILD_DIR/$PROJECT/$RELEASE" ]; then
  rm -Rf "$BUILD_DIR/$PROJECT/$RELEASE" # removing possible uncompleted stuff
fi

mkdir -p "$BUILD_DIR/$PROJECT"
cd "$BUILD_DIR"
mv "$tmpdir" "$BUILD_DIR/$PROJECT/$RELEASE" # move fetched manifest repo and name it with RELEASE tag
cd "$BUILD_DIR/$PROJECT/$RELEASE"

echo "CI: syncing repos"
set -x
repo sync --no-clone-bundle
set +x

# FIXME compare the following part with the new build pipeline
#if [ "$CI_PROJECT_URL" != "$MANIFEST_URL" ]; then
#  # we are not in the manifest repo so we need to override the repo in
#  # the manifest that corresponds to the current repo
#  if [ -z "$CI_ONIRO_GIT_REPO_PATH" ]; then
#    CI_ONIRO_GIT_REPO_PATH=$CI_PROJECT_NAME
#    # currently, all meta-* repos are fetched directly in the main dir
#    # so their relative path should correspond to their name
#  fi
#  cd "$CI_ONIRO_GIT_REPO_PATH"
#  if [ -n "$CI_MERGE_REQUEST_SOURCE_PROJECT_URL" ]; then
#    # in case of a merge request from an external (forked) repo,
#    # add that repo as remote and fetch it
#    echo "CI: Bootstrapping '$CI_PROJECT_DIR' as 'incoming-merged' remote in #'$CI_ONIRO_GIT_REPO_PATH'"
#    git remote add incoming-merged "$CI_PROJECT_DIR"
#    git fetch incoming-merged HEAD
#  fi
#  echo "CI: Switching $CI_ONIRO_GIT_REPO_PATH to $CI_COMMIT_SHA"
#  git checkout "$CI_COMMIT_SHA"
#  cd -
#fi

touch "$BUILD_DIR/$PROJECT/$RELEASE/.success-prepare"
echo
echo "CI: successfully cloned sources at $BUILD_DIR/$PROJECT/$RELEASE"
write_env
