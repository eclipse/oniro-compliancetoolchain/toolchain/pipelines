#!/bin/bash

# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>

set +e
set -o pipefail

vars="WORKDIR BUILD_TAG MAKE_TARGET DL_DIR SSTATE_DIR"
missing=""

# optional vars:
# SKIP_FAILED_BUILDS FORCE_REBUILD

for v in $vars; do
  if [ -z "${!v}" ]; then
    echo "ERROR: required env variable $v is not defined!"
    missing="y"
  fi
done
if [ -n "$missing" ]; then
  echo "aborting"
  exit 1
fi

cd "$WORKDIR"

if [ -f ".success-build-$BUILD_TAG-$MAKE_TARGET" ]; then
  echo "this target has been already successfully built, skipping"
  exit 0
fi

if [ ! -d build_$BUILD_TAG ]; then
  mkdir build_$BUILD_TAG
  for f in *; do ln -s $PWD/$f build_$BUILD_TAG/; done
  rm build_$BUILD_TAG/build_*
  ln -s $PWD/.repo build_$BUILD_TAG/
  cd build_$BUILD_TAG/
  sed -i -E 's/\$\(update_local_conf\)/$(update_local_conf) \&\& echo -e "BB_GENERATE_MIRROR_TARBALLS = \\"1\\"\\nINHERIT += \\"cve-check\\"" >> $(LOCAL_CONF) \&\& sed -i -E "s|^BB_NUMBER_THREADS|#BB_NUMBER_THREADS|;s|^PARALLEL_MAKE|#PARALLEL_MAKE|" $(LOCAL_CONF)/' Makefile
else
  cd build_$BUILD_TAG/
fi

[[ -n "$FORCE_REBUILD" ]] && rm -Rf build/*.lock build/*.sock 2>&1 >/dev/null

export DEBIAN_FRONTEND="noninteractive"
echo y | make apt-prereqs

source $PWD/.pyvenv/bin/activate

make $MAKE_TARGET

cd "$WORKDIR"

touch ".success-build-$BUILD_TAG-$MAKE_TARGET"
